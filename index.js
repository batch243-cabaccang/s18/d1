const nameFunction = (name) => {
  console.log(name);
};

nameFunction("George");
nameFunction("Horhe");

const remainedFunction = (num) => {
  console.log(num % 8 === 0);
};
remainedFunction(8);

const remainedFunction2 = (num) => {
  return num % 8 === 0;
};
const tester = remainedFunction(8);
console.log(tester);
